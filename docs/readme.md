fossbotpy
======

A simple, easy to use, non-restrictive, synchronous Fosscord API Wrapper for Selfbots/Userbots written in Python.

[![version](https://badge.fury.io/py/fossbotpy.svg)](https://badge.fury.io/py/fossbotpy) [![python versions](https://img.shields.io/badge/python-2.7%20%7C%203.5%20%7C%203.6%20%7C%203.7%20%7C%203.8%20%7C%203.9-green)](https://pypi.org/project/fossbotpy) 

Table of Contents
-----------------

-   [Using fossbotpy (make selfbots and userbots)](using.md)  
    -   [fetching guild members](using/fetchingGuildMembers.md)
-   [Extending fossbotpy (add fosscord API wraps)](extending.md)
